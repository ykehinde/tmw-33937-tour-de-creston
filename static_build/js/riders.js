/**
	fabz riders
 */



;( function( Riders, $) {


	'use strict';

	Riders.Config = {

		path : "riders_feed.json",
		informationArray: {},

		$ridersContainer		: $(".riders-container"),

		init : function () {

			Riders.Config.getInformation();
		},
			 getInformation :  function () {

			//	console.log("Loading ....");
				$.getJSON(Riders.Config.path, function(data) 
				{
					Riders.Config.informationArray = data;
					Riders.Config.populateRidersPage(Riders.Config.informationArray);
				});
			},
			populateRidersPage : function (data) 
				{ 
					// var data = Riders.Config.informationArray;
					var ridersMarkup = ''; 

					for (var i = 0; i < data.Riders.length; i++) {


						ridersMarkup	+= '<figure class="rider-info">';
						ridersMarkup	+= '<img class="rider-info--image" src="img/'+data.Riders[i].picture+'">';
						ridersMarkup	+= '<figcaption class="rider-info--caption">';
						ridersMarkup	+= '<ul class="rider-info--data">';
						ridersMarkup	+= '<li class="rider-info--name">Name: <span>'+data.Riders[i].name+'</span></li>';
						ridersMarkup	+= '<li class="rider-info--nickname">Nickname: <span>'+data.Riders[i].nickname+'</span></li>';
						ridersMarkup	+= '<li >Chosen Charity: <span class="rider-info--charityname">'+data.Riders[i].charity+'</span>&nbsp;<a href="'+data.Riders[i].charitylink+'" target="_blank" class="btn btn--natural rider-info--donate">Donate</a></li>';
						ridersMarkup	+= '<li>Bike: <span class="rider-info--bike">'+data.Riders[i].bike+'</span></li>';
						ridersMarkup	+= '<li>About: <span class="rider-info--about">'+ data.Riders[i].about+'</span></li>';
						ridersMarkup	+='</ul>';
						ridersMarkup	+='</figcaption>';
						ridersMarkup	+='</figure>';
					};
					Riders.Config.$ridersContainer.append(ridersMarkup);
				}
	};


} )( window.Riders = window.Riders || {}, jQuery);