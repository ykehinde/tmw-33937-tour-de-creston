# README #

### What is this repository for? ###

* Tour-de-Creston rider registration and social hub for Creston staff and Creston riders.

### How do I get set up? ###

* Used to be a static HTML project. Now converted to a .NET project
* Static files can be found in the 'static_build' folder
* .NET project can be found in the trunk folder
* Resources point back to static folder containing compiled css and js

* Runs on grunt (old version)

### Current State ###
* Form built. Database needed to be populated. Once populated, data should be able to be pulled for rider info and social hub.
* Routes page built but actual data not added
* Kit page added but subject to change next year
* Home page added, info will need to be changed next year

### Who do I talk to? ###

* Yemi Kehinde (yemi@kehinde.me)
* Ciaran Park