﻿using TourDeCreston.Datalayer;
using TourDeCreston.Datalayer.Interfaces;
using TourDeCreston.Domain.Objects;

using TourDeCreston.Services.Interfaces;

namespace TourDeCreston.Services
{
    public class AccountService : IAccountService
    {
        private readonly ITourDeCrestonRepository _crestonRepository;

        public AccountService(ITourDeCrestonRepository crestonRepository)
        {
            _crestonRepository = crestonRepository;
        }

        public RegisterResponse Register(TdcRider rider)
        {

            var response = _crestonRepository.RegisterRider(rider);

            return new RegisterResponse()
            {
                Success = response.Success

            };

        }
    }
}