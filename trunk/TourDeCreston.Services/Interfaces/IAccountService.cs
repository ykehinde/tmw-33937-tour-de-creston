﻿using TourDeCreston.Domain.Objects;

namespace TourDeCreston.Services.Interfaces
{
    public interface IAccountService
    {

        RegisterResponse Register(TdcRider rider);
    }
}
