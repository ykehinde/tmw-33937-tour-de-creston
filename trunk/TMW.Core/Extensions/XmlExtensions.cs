﻿using System.Linq;
using System.Xml.Linq;

namespace TMW.Core.Extensions
{
    public static class XmlExtensions
    {
        public static string AttributeValue(this XElement element, string attribute)
        {
            if (element != null && element.Attributes(attribute).Any())
                return element.Attribute(attribute).Value;
            return string.Empty;
        }

        public static string ChildValue(this XElement parent, string childName)
        {
            var element = parent.Element(childName);
            if (element != null)
                return element.Value;
            return string.Empty;
        }


    }
}