﻿using System;

namespace TMW.Core.Extensions
{
	public static class DateExtensions
	{
		public static string ToTimeAgo(this DateTime date)
		{
			var ts = new TimeSpan(DateTime.UtcNow.Ticks - date.ToUniversalTime().Ticks);
			double delta = Math.Abs(ts.TotalSeconds);

			//Future
			if (delta < 0)
				return "not yet";

			//Now
			if (delta == 0)
				return "now";

			//Second
			if (delta < 1 * 60)
				return ts.Seconds == 1 ? "one second ago" : ts.Seconds + " seconds ago";

			//A minute ago
			if (delta < 2 * 60)
				return "a minute ago";

			//Minutes
			if (delta < 45 * 60)
				return ts.Minutes + " minutes ago";

			//An hour ago
			if (delta < 90 * 60)
				return "an hour ago";

			//Hours
			if (delta < 24 * 60 * 60)
				return ts.Hours + " hours ago";

			//A day ago
			if (delta < 48 * 60 * 60)
				return "yesterday";

			//Days
			if (delta < 30 * 24 * 60 * 60)
				return ts.Days + " days ago";

			//A month ago
			if (delta < 12 * 30 * 24 * 60 * 60)
			{
				int months = Convert.ToInt32(Math.Floor((double)ts.Days / 30));
				return months <= 1 ? "one month ago" : months + " months ago";
			}

			//Years
			int years = Convert.ToInt32(Math.Floor((double)ts.Days / 365));
			return years <= 1 ? "one year ago" : years + " years ago";
		}
	}
}
