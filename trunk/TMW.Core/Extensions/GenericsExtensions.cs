﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TMW.Core.Extensions
{
	public static class GenericsExtensions
	{
		public static T PickRandom<T>(this IEnumerable<T> source)
		{
			return source.PickRandom(1).Single();
		}

		public static IEnumerable<T> PickRandom<T>(this IEnumerable<T> source, int count)
		{
			return source.Shuffle().Take(count);
		}

		public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> source)
		{
			return source.OrderBy(x => Guid.NewGuid());
		}

		public static IEnumerable<IEnumerable<T>> Split<T>(this IEnumerable<T> list, int parts)
		{
			var splits = new List<List<T>>();

			var listLength = list.Count() / parts;
			var remainder = list.Count() % parts;

			var position = 0;

			for (var i = 0; i < parts; i++)
			{
				var extra = remainder - i > 0 ? 1 : 0;

				splits.Add(list.Skip(position).Take(listLength + extra).ToList());
				position += listLength + extra;
			}

			return splits;
		}
	}
}
