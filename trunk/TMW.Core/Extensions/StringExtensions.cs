﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Xml.Serialization;

namespace TMW.Core.Extensions
{
    public static class StringExtensions
    {
        public static String[] LowecaseWords =
            {
                "and", "are", "as", "at", "be", "but", "by",
                "for", "if", "in", "into", "is", "it",
                "not", "of", "on", "or", "such",
                "that", "the", "their", "then", "there", "these",
                "they", "this", "to", "was", "will", "with", "a",
                "und", "sind", "wie", "bei", "sein", "aber", "durch", 
                "für", "wenn", "falls", "in", "ist", "es", "nicht", "von", 
                "am", "oder", "so", "dass", "der", "die", "das", "ihr", 
                "dann", "dort", "diese", "sie", "dieses", "zu", 
                "war", "wird", "mit", "ein", "eine"
            };

		private static List<string> SpecialChars = new List<string>() { "!", "\"", "£", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "]", "{", "}", ":", ";", "@", "~", "#", "<", ">", ",", ".", "/", "?", "¬", "`", "¦", "|", "\\", };


        public static bool IsEmpty(this string str)
        {
            return string.IsNullOrWhiteSpace(str);
        }

      

        public static bool IsNotEmpty(this string str)
        {
            return string.IsNullOrWhiteSpace(str) == false;
        }

        public static bool EqualsTo(this string str, string other, bool caseSensitive = false)
        {
            return string.Equals(str, other,
                                 caseSensitive
                                     ? StringComparison.CurrentCulture
                                     : StringComparison.CurrentCultureIgnoreCase);
        }

        public static string StripTrailingSlash(this string text)
        {
            if (text.IsEmpty())
                return text;

            return text.TrimEnd('/', ' ');
        }

        public static bool HasHtmlTags(this string text)
        {
            return Regex.IsMatch(text, "<.*?>");
        }

        public static string RemovePluralExtension(this string text)
        {
            if (text.IsEmpty())
                return "";
            return Regex.Replace(text, @"\(.*?\)$", "");
        }

        public static string RemovePortFromUrl(this string url)
        {
            if (url.IsEmpty()) return url;
            var uri = new Uri(url);
            return uri.GetComponents(UriComponents.AbsoluteUri & ~UriComponents.Port,
                               UriFormat.UriEscaped);
        }

		public static int GetIndexOfFirstAlphabeticCharacter(this string text)
		{
			var match = Regex.Match(text, "[A-Za-z]");
			if (match.Success)
			{
				return match.Index;
			}

			return 0;
		}


        public static string ToTitleCase(this string text)
        {
            if (text.IsEmpty())
                return text;

            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
            TextInfo textInfo = cultureInfo.TextInfo;

            var formattedTitle = textInfo.ToTitleCase(HttpUtility.HtmlDecode(text)).Trim();
            foreach (var word in LowecaseWords)
            {
                formattedTitle = Regex.Replace(formattedTitle, @"([\s.\-,])+" + word + @"(([\s.\-,])+|$)", "$1" + word + "$2",
                                               RegexOptions.IgnoreCase);
            }

            return formattedTitle;
        }

        public static string SafeToLower(this string text)
        {
            if (text.IsEmpty())
                return text;
            return text.ToLower();
        }

        public static string RemoveSpecialChars(this string text)
        {
            if (text.IsEmpty())
                return string.Empty;
            return Regex.Replace(HttpUtility.HtmlDecode(text), @"[^\w\s\d-]", "");
        }

        public static string FixTitleSpecialCharacters(this string text)
        {
            if (text.IsEmpty())
                return string.Empty;
            return HttpUtility.HtmlDecode(text);
        }

        public static string GenerateSlug(this string phrase, bool decodeFirst = false)
        {
            // invalid chars           
            string text = decodeFirst ? HttpUtility.HtmlDecode(phrase) : phrase;
            text = RemoveSpecialChars(text);
            if (text.IsEmpty())
                return string.Empty;

            text = text.RemoveAccent().ToLower();
            // convert multiple spaces into one space   
            text = Regex.Replace(text, @"\s+", " ").Trim();
            // cut and trim 
            text = Regex.Replace(text, @"\s", "-"); // hyphens
            return text;
        }
        public static string GenerateEncodedSlug(this string phrase, bool decodeFirst = false)
        {
            // invalid chars           
            string text = decodeFirst ? HttpUtility.UrlEncode(phrase) : phrase;
            text = RemoveSpecialChars(text);
            if (text.IsEmpty())
                return string.Empty;

            //text = text.RemoveAccent().ToLower();
            // convert multiple spaces into one space   
            text = Regex.Replace(text, @"\s+", " ").Trim();
            // cut and trim 
            text = Regex.Replace(text, @"\s", "-"); // hyphens
            return text;
        }
        public static string StripSpecialCharacters(this string input)
        {
            if (input.IsEmpty())
            {
                return input;
            }
            return Regex.Replace(input, @"[|&;$%@""<>\(\)\+\n\r,\\]", "");
        }

		public static string RemoveSpecialCharacters(this string input)
		{
			if (input.IsEmpty())
			{
				return input;
			}
			input = SpecialChars.Aggregate(input, (current, specialChar) => current.Replace(specialChar, string.Empty));
			return input;
		}

		public static bool ContainsSpecialCharacters(this string input)
		{
			if (input.IsEmpty())
			{
				return false;
			}

			if (SpecialChars.Any(specialChar => input.Contains(specialChar)))
			{
				return true;
			}

			return false;
		}

	    public static string EncodeToBase64(this string text,Encoding encoding = null)
		{
			var usedEncoding = encoding ?? ASCIIEncoding.ASCII;
			byte[] toEncodeAsBytes = usedEncoding.GetBytes(text);
			var baseString = Convert.ToBase64String(toEncodeAsBytes);
			return baseString;
		}

		public static string DecodeFromBase64(this string text,Encoding encoding = null)
		{
			var usedEncoding = encoding ?? ASCIIEncoding.ASCII;
			byte[] encodedDataAsBytes =Convert.FromBase64String(text);

			string returnValue = encoding.GetString(encodedDataAsBytes);

			return returnValue;
		}

        private static string RemoveAccent(this string txt)
        {
            byte[] bytes = System.Text.Encoding.GetEncoding("Cyrillic").GetBytes(txt);
            return System.Text.Encoding.ASCII.GetString(bytes);
        }

		public static T ToObject<T>(this string xmlText)
		{
			using (var stringReader = new System.IO.StringReader(xmlText))
			{
				var serializer = new XmlSerializer(typeof(T));
				return (T)serializer.Deserialize(stringReader);	
			}
		}

		public static string Encrypt(this string currentString,string salt,string RFCPassword)
		{
			int Rfc2898KeygenIterations = 100;
			int AesKeySizeInBits = 128;
			byte[] Salt = Encoding.Default.GetBytes(salt);
			byte[] rawPlaintext = Encoding.Unicode.GetBytes(currentString);
			byte[] cipherText = null;
			using (Aes aes = new AesManaged())
			{
				aes.Padding = PaddingMode.PKCS7;
				aes.KeySize = AesKeySizeInBits;
				int KeyStrengthInBytes = aes.KeySize / 8;
				Rfc2898DeriveBytes rfc2898 =
					new Rfc2898DeriveBytes(RFCPassword, Salt, Rfc2898KeygenIterations);
				aes.Key = rfc2898.GetBytes(KeyStrengthInBytes);
				aes.IV = rfc2898.GetBytes(KeyStrengthInBytes);
				using (MemoryStream ms = new MemoryStream())
				{
					using (CryptoStream cs = new CryptoStream(ms, aes.CreateEncryptor(), CryptoStreamMode.Write))
					{
						cs.Write(rawPlaintext, 0, rawPlaintext.Length);
						cs.FlushFinalBlock();
					}
					cipherText = ms.ToArray();
				}
			}
			return Convert.ToBase64String(cipherText);
		}

		public static string Decrypt(this string currentString, string salt, string RFCPassword)
		{
			int Rfc2898KeygenIterations = 100;
			int AesKeySizeInBits = 128;
			byte[] Salt = Encoding.Default.GetBytes(salt);
			byte[] cipherText = Convert.FromBase64String(currentString);
			byte[] plainText = null;
			using (Aes aes = new AesManaged())
			{
				aes.Padding = PaddingMode.PKCS7;
				aes.KeySize = AesKeySizeInBits;
				int KeyStrengthInBytes = aes.KeySize / 8;
				Rfc2898DeriveBytes rfc2898 =
					new Rfc2898DeriveBytes(RFCPassword, Salt, Rfc2898KeygenIterations);
				aes.Key = rfc2898.GetBytes(KeyStrengthInBytes);
				aes.IV = rfc2898.GetBytes(KeyStrengthInBytes);
				
				using (MemoryStream ms = new MemoryStream())
				{
					using (CryptoStream cs = new CryptoStream(ms, aes.CreateDecryptor(), CryptoStreamMode.Write))
					{
						cs.Write(cipherText, 0, cipherText.Length);
						//cs.FlushFinalBlock();
					}
					plainText = ms.ToArray();
				}

			}
			return Encoding.Unicode.GetString(plainText);
		}


    }
}