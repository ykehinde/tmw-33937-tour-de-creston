﻿using System;

namespace TMW.Core.Extensions
{
    public static class DecimalExtensions
    {
        public static decimal HalfRoundUp(this decimal value)
        {
            var rounded = Math.Round(value, 2);
            var floor = Math.Floor(rounded);
            var points = (rounded - floor) * 100;

            decimal returnValue;

            if (points <= 25)
            {
                returnValue = floor;
                return returnValue;
            }

            if (points <= 50)
            {
                returnValue = floor + 0.5M;
                return returnValue;
            }

            if (points <= 75)
            {
                returnValue = floor + 0.5M;
                return returnValue;
            }

            if (points > 75)
            {
                returnValue = floor + 1;
                return returnValue;
            }
            return value;
        }
    }
}