﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;

namespace TMW.Core.Extensions
{
    public static class NameValueCollectionExtensions
    {
        public static string ToQueryString(this NameValueCollection nvc, bool excludeQuestionMark = false)
        {
            if (nvc == null || nvc.AllKeys.Length == 0)
            {
                return "";
            }
            
            return (excludeQuestionMark ? "" : "?") + string.Join("&", Array.ConvertAll(nvc.AllKeys, key => string.Format("{0}={1}", WebUtility.UrlEncode(key), WebUtility.UrlEncode(nvc[key]))));
        }
    }

    public static class DictionaryExtensions
    {
        public static string ToQueryString(this IDictionary<string, string> dict, bool excludeQuestionMark = false)
        {
            if (dict == null || dict.Count == 0)
            {
                return "";
            }

            return (excludeQuestionMark ? "" : "?") + string.Join("&", dict.Keys.Select(key => string.Format("{0}={1}", WebUtility.UrlEncode(key), WebUtility.UrlEncode(dict[key]))));
        }
    }
}
