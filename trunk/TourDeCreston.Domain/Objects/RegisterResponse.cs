﻿using System.Collections.Generic;

namespace TourDeCreston.Domain.Objects
{
    public class RegisterResponse
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public IEnumerable<TdcRider> Riders { get; set; }
    }
        public class RidersDbResponse
    {
            public bool Success { get; set; }
            public IEnumerable<TdcRider> Riders { get; set; }
    }
}