﻿namespace TourDeCreston.Domain.Objects
{
    public class TdcRider
    {
        public int Id { get; set; }
        public string Firstname { get; set; }
        public string Surname { get; set; }
        public string EmailAddress { get; set; }
    }
}