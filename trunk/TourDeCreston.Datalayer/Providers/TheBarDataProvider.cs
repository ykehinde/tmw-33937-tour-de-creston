﻿using System;
using System.Data.Entity;
using System.Linq;
using TourDeCreston.Datalayer.Interfaces;

namespace TourDeCreston.Datalayer.Providers
{
    public class TourDeCrestonProvider : IDataProvider, IDisposable
    {
        private readonly TDCEntities  _database;

        public TourDeCrestonProvider(TDCEntities database)
        {
            var type = typeof(System.Data.Entity.SqlServer.SqlProviderServices);
            _database = database;
            
            _database.Configuration.LazyLoadingEnabled = true;
            _database.Configuration.AutoDetectChangesEnabled = false;
        }

        public void Dispose()
        {
            _database.Dispose();
        }


        public TDCEntities GetContext()
        {
            return _database;
        }


        public void Update<T>(T entity) where T : class
        {            
            //_database.Entry(entity).State = System.Data.Entity.EntityState.Modified;  
            _database.SaveChanges();
        }

        public T GetContext<T>() where T : DbContext
        {
            return _database as T;
        }

        public void SaveChanges()
        {
            var configuration = _database.Configuration;
            _database.SaveChanges();
        }

        public void Add<T>(T t, bool saveImmediatelyAfterAdd = false) where T : class
        {
            _database.Set<T>().Add(t);
            
            //_database.Entry(t).State = System.Data.Entity.EntityState.Added;  
            if (saveImmediatelyAfterAdd)
            {
                SaveChanges();

            }
        }

        public void Remove<T>(T t)
            where T : class
        {
            _database.Set<T>().Remove(t);
            SaveChanges();
        }

        public IQueryable<T> GetAll<T>() where T : class
        {
            return _database.Set<T>();
        }

    }
}