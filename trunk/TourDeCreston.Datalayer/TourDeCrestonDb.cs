﻿using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using TMW.Core.Extensions;
using TourDeCreston.Datalayer.Interfaces;
using TourDeCreston.Domain.Objects;

namespace TourDeCreston.Datalayer
{
    public class TourDeCrestonRespository: ITourDeCrestonRepository
    {
        private readonly IDataProvider _dataProvider;


        public TourDeCrestonRespository(IDataProvider dataProvider)
        {
            _dataProvider = dataProvider;
        }


        public RidersDbResponse RegisterRider(TdcRider rider)
        {
            try
            {
                if (rider != null)
                {
                    _dataProvider.Add(rider.MapTo<Rider>());
                    return new RidersDbResponse() {Success = true};
                }
                
            }
            catch (Exception exception)
            {
                return new RidersDbResponse() {Success = false };
            }
            return new RidersDbResponse() { Success = false};
        }
    }
}
