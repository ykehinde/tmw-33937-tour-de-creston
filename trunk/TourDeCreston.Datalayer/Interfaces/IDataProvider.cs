﻿using System.Data.Entity;
using System.Linq;

namespace TourDeCreston.Datalayer.Interfaces
{
    public interface IDataProvider
    {
        void Add<T>(T t, bool saveImmediatelyAfterAdd = false) where T : class;
        void Update<T>(T entity) where T : class;
        void Remove<T>(T t) where T : class;
        IQueryable<T> GetAll<T>() where T : class;
        T GetContext<T>() where T : DbContext;
        void SaveChanges();
    }
}
