﻿using TourDeCreston.Domain.Objects;

namespace TourDeCreston.Datalayer.Interfaces
{
    public interface ITourDeCrestonRepository
    {
        RidersDbResponse RegisterRider(TdcRider rider);

    }
}