﻿using AutoMapper;
using TourDeCreston.Domain.Objects;

namespace TourDeCreston.Datalayer.MappingProfiles
{
    public class  DataModelProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<TdcRider, TdcRider>().ReverseMap();
        }
    }
}