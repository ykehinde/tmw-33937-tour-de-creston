﻿using System;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using TourDeCreston.Domain.Objects;
using TourDeCreston.Web.Models;

namespace TourDeCreston.Web.Infrastructure.MappingProfiles
{
    public class  ViewModelsProfile : Profile
    {
      
        protected override void Configure()
        {

            Mapper.CreateMap<RegisterViewModel, TdcRider>().ReverseMap();
        }
    }
}