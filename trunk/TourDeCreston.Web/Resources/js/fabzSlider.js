/**
	fabz slider
 */



;( function( FabzSlider, $) {


	'use strict'

	$(function() {

		FabzSlider.Config.init();

	});

	$(window).resize(function() {

		FabzSlider.Config.setSliderHeight( $(".fabzSlider").find('ul').find('li').find('picture').height());
	//	console.log("resize to : ",FabzSlider.Config.$sliderContainerHeight );
		FabzSlider.Config.reEvaluteImages();
	});

	FabzSlider.Config = {

		//slider config values
		readingTime					: 4000, // time that takes the slide to change
		displayDots					: true, // boolean to add or hide the dots 
		activeSlide 				: 0, // this determines the initial slide
		$sliderContainer			: $(".fabzSlider"),
		$sliderContainerUl 			: $(".fabzSlider").find('ul'),
		$sliderContainerLi			: $(".fabzSlider").find('ul').find('li'),
		$sliderContainerHeight		: $(".fabzSlider").find('ul').find('li').find('picture').height(),
		$sliderContainerItemsCount 	: $(".fabzSlider").find('ul').find('li').length,
		$sliderContainerPlay 		: $(),
		timeInterval				: {},
		isTransitioning				: false,


	//	$sliderContainerItems : FabzSlider.Config.$sliderContainer.children( 'li' ),
	//	$sliderContainerTotalNumber :  FabzSlider.Config.$sliderContainerItems.length,
//
		init : function () {
			console.debug('FabzSlider is running');

			FabzSlider.Config.setSliderHeight( $(".fabzSlider").find('ul').find('li').find('picture').height());

			FabzSlider.Config.createSliderDots(FabzSlider.Config.displayDots);

			FabzSlider.Config.activateTargetSlide(FabzSlider.Config.activeSlide);

			FabzSlider.Config.animateSlides();

			FabzSlider.Config.reEvaluteImages();
			// console.log("$sliderContainer : ",FabzSlider.Config.$sliderContainer,
			// 	"$sliderContainerUl : ",FabzSlider.Config.$sliderContainerUl,
			// 	"$sliderContainerLi : ",FabzSlider.Config.$sliderContainerLi,
			// 	"$sliderContainerHeight : ",FabzSlider.Config.$sliderContainerHeight
			// 	"$sliderContainerItems : ",FabzSlider.Config.$sliderContainerItems,
			// 	"$sliderContainerTotalNumber :",FabzSlider.Config.$sliderContainerTotalNumber
			//	);
		},

		setSliderHeight : function (value) {

			// store the new value
			FabzSlider.Config.$sliderContainerHeight = value;
			//apply it to the container 
			FabzSlider.Config.$sliderContainer.height(FabzSlider.Config.$sliderContainerHeight);
		//	console.log(value);
		},

		createSliderDots : function (boolean) {

			if(boolean) {

				// grab the container
				var $sliderContainer = FabzSlider.Config.$sliderContainer.find('.circular-button-container');
				//and for each li
				$(".fabzSlider").find('ul').find('li').each(
					function(i) {
						//we add a new dot
						$sliderContainer.append("<div class='circular-button'></div>");
				//		console.log($sliderContainer.find('.circular-button'));
					}
				);
			//	console.log("the other one : ",$sliderContainer.find('.circular-button'));
				$sliderContainer.find('.circular-button').click(FabzSlider.Config.onDotClick);
			}
		},
		onDotClick:function (evt) { 

			if (FabzSlider.Config.isTransitioning === false ) {
			//	console.log(FabzSlider.Config.$sliderContainer.find('.circular-button').index(evt.currentTarget));
				FabzSlider.Config.activateTargetSlide(FabzSlider.Config.$sliderContainer.find('.circular-button').index(evt.currentTarget));
				FabzSlider.Config.stopTimerInterval();

				// create the play button
				FabzSlider.Config.createPlayButton();

			}else { 


			}
		},

		createPlayButton:function () { 

				// remove any play button
				$('.circular-button-play-container').find('.circular-play').remove();
				
				// grab the container
				var $sliderContainerPlay = $('.circular-button-play-container');

				// add a new play button
				$sliderContainerPlay.append("<div class='circular-play'>&#9658;</div>");
				// and events
				$sliderContainerPlay.find('.circular-play').click(FabzSlider.Config.onPlayClick);
		},

		onPlayClick:function () {

			// kill the play button
			$('.circular-button-play-container').find('.circular-play').remove();

			
			FabzSlider.Config.animateSlides(FabzSlider.Config.activeSlide);
			FabzSlider.Config.timerInterval();
		},

		activateTargetSlide:function (slideNum) { 
		//	console.log("slideNum : ",slideNum);

			FabzSlider.Config.activeSlide = slideNum;

			$(".fabzSlider").find('ul').find('li').each(
				function(i,object) {

				//	console.dir($(object).children(".slide"));
					var currentIndex = FabzSlider.Config.$sliderContainerItemsCount-1 -i;
					var $currentDot = $(FabzSlider.Config.$sliderContainer.find('.circular-button-container').find('.circular-button').get(currentIndex));
				//	console.log($currentDot);

					if (slideNum === currentIndex) {

						$(object).children(".slide").css('opacity',1);
			//			console.log("match : ", currentIndex);
						$currentDot.addClass('active');

					}else {

						$(object).children(".slide").css('opacity',0);
						$currentDot.removeClass('active');

					}

				}
			);
		},

		animateSlides:function () {
			FabzSlider.Config.timeInterval = setInterval(function(){ FabzSlider.Config.timerInterval() }, FabzSlider.Config.readingTime);
		},

		timerInterval:function () {

			if(FabzSlider.Config.activeSlide < FabzSlider.Config.$sliderContainerItemsCount-1 ) {

				FabzSlider.Config.activeSlide  = FabzSlider.Config.activeSlide +1;
				FabzSlider.Config.activateTargetSlide(FabzSlider.Config.activeSlide ++ );
			} else { 

				FabzSlider.Config.activateTargetSlide(0);

			}

		},

		stopTimerInterval:function () {
			clearInterval(FabzSlider.Config.timeInterval);
		},

		reEvaluteImages: function() { 

			picturefill( {
				reevaluate: true
			});
		},

	};


} )( window.FabzSlider = window.FabzSlider || {}, jQuery);