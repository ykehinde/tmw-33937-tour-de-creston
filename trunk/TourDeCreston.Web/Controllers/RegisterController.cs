﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TMW.Core.Extensions;
using TourDeCreston.Domain.Objects;
using TourDeCreston.Services.Interfaces;
using TourDeCreston.Web.Models;

namespace TourDeCreston.Web.Controllers
{
    public class RegisterController : Controller
    {
        private readonly IAccountService _accountService;

        private static string[] _crestonCompanies = {"TMW Unlimited", "Creston", "Things", "Nelson Bostock", "ICM"};
        private static string[] _preferredRoute = { "Social", "Intermediate", "Fast", };
        public RegisterController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        [HttpGet]
        public ActionResult Index()
        {

            var crestonCompaniesSelectListitems = _crestonCompanies.MapTo<SelectListItem>().OrderBy(x => x.Text).ToList();
            var preferredRouteSelectListitems = _crestonCompanies.MapTo<SelectListItem>().OrderBy(x => x.Text).ToList();

            crestonCompaniesSelectListitems.Insert(0, new SelectListItem() { Text = "Select One", Value = string.Empty });
            preferredRouteSelectListitems.Insert(0, new SelectListItem() { Text = "Select One", Value = string.Empty });
            ViewBag.CrestonCompanies = crestonCompaniesSelectListitems;
            ViewBag.PreferredRoutes = preferredRouteSelectListitems;
            
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(RegisterViewModel model)
        {

            // validate
            if (ModelState.IsValid)
            {
                // if valid, save registered user to db

                var response = _accountService.Register(model.MapTo<TdcRider>());
                if (response.Success)
                {
                    // return to thank you page    
                    return View("Thanks");
                }
                ModelState.AddModelError(string.Empty, response.Message);
            }
             // else show errors on view
            return View(model);           
        }


    }
}