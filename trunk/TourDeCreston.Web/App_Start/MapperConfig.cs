﻿using AutoMapper;
using StructureMap;
using TourDeCreston.Datalayer.MappingProfiles;
using TourDeCreston.Web.Infrastructure.MappingProfiles;

namespace TourDeCreston.Web
{
    public class MapperConfig
    {
        public static void Configure()
        {
            Mapper.Initialize(delegate { });
            Mapper.AddProfile(ObjectFactory.GetInstance<ViewModelsProfile>());
            Mapper.AddProfile(ObjectFactory.GetInstance<DataModelProfile>());
        }
    }
}